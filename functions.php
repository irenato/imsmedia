<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 13.11.17
     * Time: 23:53
     */
    require_once('includes/styles.php');
    if (is_admin()) {
        require_once('includes/hidden.php');
        require_once('includes/custom_admin.php');
        require_once('includes/custom_posts.php');
        require_once('includes/role.php');
        require_once('includes/exp.php');
        require_once('includes/request.php');
    }

    add_theme_support('post-thumbnails');

    function themeslug_theme_customizer($wp_customize)
    {
        $wp_customize->add_section('themeslug_logo_section', [
            'title'       => __('Logo', 'themeslug'),
            'priority'    => 30,
            'description' => 'Upload a logo to replace the default site name and description in the header',
        ]);

        $wp_customize->add_setting('themeslug_logo');
        $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'themeslug_logo', [
            'label'    => __('Logo', 'themeslug'),
            'section'  => 'themeslug_logo_section',
            'settings' => 'themeslug_logo',
        ]));
    }

    /**
     * @param $url
     * @param $request
     * @param $user
     * блокирование доступа в админку для пользователя
     * заблокированного (либо с отр. баллансом)
     */
    /*
    function my_login_redirect($url, $request, $user)
    {
        if ($user->roles[0] == 'partner' &&
            (get_field('blocked', 'user_' . $user->data->ID)
                || get_field('ballance', 'user_' . $user->data->ID) <= 0)
        ) {
            $logout_url = wp_login_url() . '?mode=maintainance';
            wp_logout();
            wp_redirect($logout_url, 302);
            die();
        }

    }


    add_filter('login_redirect', 'my_login_redirect', 10, 3);
    */
    /**
     * @return string
     * сообщение для заблокированного пользователя
     */
    function show_login_error_message()
    {
        if ($_GET['mode'] == 'maintainance') {
            return '<p class="message"><b>Ваш аккаунт заблокирован</b><br>'
                . 'для подробностей свяжитесь с администратором: '
                . '<a href="mailto:' . get_option('admin_email') . '">'
                . get_option('admin_email') . '</a></p>';
        }
    }

    add_filter('login_message', 'show_login_error_message');

    /**
     * @param $value
     * @param $post_id
     * @param $field
     *
     * @return mixed
     *
     * действия при изменении балланса пользователя
     * при отрицательном - блокирование и разлогинивание
     * при положительном разблокирование
     */
    function my_acf_update_value($value, $post_id, $field)
    {

        if ($field['name'] == 'ballance' && $value <= 0) {
            update_field('blocked', true, $post_id);
            // разлогинивание пользователя
//            $sessions = WP_Session_Tokens::get_instance(str_replace('user_', '', $post_id));
//            $sessions->destroy_all();
        } elseif ($field['name'] == 'ballance' && $value > 0) {
            update_field('blocked', false, $post_id);
        }
        return $value;

    }

    add_filter('acf/update_value', 'my_acf_update_value', 10, 3);

    function create_fake_leads()
    {
        global $wpdb;
        for ($i = 0; $i <= 99; ++$i) {
            $wpdb->insert($wpdb->prefix . 'ims_leads', [
                'user_id'      => rand(2, 4),
                'client_ip'    => rand(000, 999) . '-'
                    . rand(000, 999) . '-'
                    . rand(0, 999) . '-'
                    . rand(0, 999),
                'client_phone' => '+' . rand(1, 9)
                    . '(' . rand(000, 999) . ') '
                    . rand(000, 999) . '-'
                    . rand(00, 99) . '-'
                    . rand(00, 99),
                'status'       => rand(0, 3),
            ]);
        }
    }

//    add_action('admin_init', 'create_fake_leads');

    function show_products_categories()
    {
        $args   = [
            'type'         => 'post',
            'child_of'     => false,
            'parent'       => '',
            'orderby'      => 'name',
            'order'        => 'ASC',
            'hide_empty'   => 1,
            'hierarchical' => 1,
            'exclude'      => 1,
            'number'       => 0,
            'taxonomy'     => 'category',
            'pad_counts'   => false,
        ];
        $result = [];
        if ($categories = get_categories($args)) {
            foreach ($categories as $category) {
                if ($category->parent) {
                    $result[$category->parent]['childs'][] = $category;
                } else {
                    $result[$category->term_id]['item'] = $category;
                }
            }
        }
        return $result;
    }

    /**
     * @param        $post_id
     * @param string $field
     * @param int    $count
     *
     *  счетчик показа поста
     */
    function update_post_views($post_id, $field = 'views')
    {
        $count = (int)get_post_meta($post_id, $field, true);
        if (!update_post_meta($post_id, $field, ($count + 1))) {
            add_post_meta($post_id, $field, 1, true);
        }
    }

    /**
     * @param $post_ID
     *
     * @return mixed
     * add_post_meta
     *
     * добавление просмотров при добавлении продукта
     */
    function set_default_meta_new_post($post_ID)
    {
        $current_field_value = get_post_meta($post_ID, 'views', true);
        if ($current_field_value == '' && !wp_is_post_revision($post_ID)) {
            add_post_meta($post_ID, 'views', 0, true);
        }
        return $post_ID;
    }

    add_action('wp_insert_post', 'set_default_meta_new_post');

    /**
     * @return mixed
     * вывод параметра поиска
     */
    function show_search_param()
    {
        if (isset($_GET['search'])) {
            return esc_html($_GET['search']);
        }
        if (isset($_GET['category'])) {
            return get_category(esc_sql($_GET['category']))->name;
        }
    }

    /**
     * @return mixed
     * результаты поиска
     */
    function get_data()
    {
        if ($_GET['search']) {
            return get_posts([
                "post_type" => "product",
                "s" => $_GET['search']
            ]);
        } elseif ($_GET['category']) {
            return get_posts([
                "post_type" => "product",
                "category" => $_GET['category']
            ]);
        }
    }

    function get_phones($user_id){
        $phones = get_field('phones', 'user_' . $user_id);
        $result = array();
        foreach ($phones as $phone){
            array_push($result, $phone['phone']);
        }
        return implode('<br>', $result);
    }


    /**
     * @param $partner_id
     *
     * @return mixed
     *
     * преимущества партнера в категория
     */
    function show_partner_advantages($partner_id, $category_id)
    {
        global $wpdb;
        return $wpdb->get_results("SELECT description, image
                        FROM `wp_user_category_advantage`
                        WHERE user_id=" . $partner_id . "
                        AND category_id=" . $category_id);
    }
