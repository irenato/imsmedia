<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 20.11.17
     * Time: 23:17
     */

    /**
     * Template name: Home
     */

    get_header();

    if (have_posts()) {
        while (have_posts()) {
            the_post();
            ?>

            <?php if ($categories = show_products_categories()): ?>
                <section class=catalog>
                    <div class=container><h2 class=section-title>Каталог товаров</h2></div>
                    <ul class=catalog-list>
                        <?php foreach ($categories as $category): ?>

                            <li class=catalog-item>
                                <?php if ($category['childs']): ?>
                                    <div class=container>
                                <span class=catalog-item__name>
                                    <?= $category['item']->name ?>
                                </span>
                                        <span class=catalog-item__right>-</span>
                                    </div>
                                    <ul class=catalog-submnu>
                                        <?php foreach ($category['childs'] as $child): ?>
                                            <li class=catalog-submnu__item><a href="<?= get_page_link(2) ?>?category=<?= $child->term_id ?>">
                                                    <div class=container><?= $child->name ?> <span>></span>
                                                    </div>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php else: ?>
                                    <div class=container>
                                        <a href="<?= get_page_link(2) ?>?category=<?= $category['item']->term_id ?>" class=catalog-item__name>
                                            <?= $category['item']->name ?>
                                        </a>
                                        <span class=catalog-item__right>-</span>
                                    </div>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </section>
            <?php endif; ?>
            <?php
            $products = get_data();
            if ($products):
                ?>
                <section>
                    <div class=container>
                        <h2 class=section-title>Результаты поиска (<?= count($products) ?>)</h2>
                        <div class="row">
                            <div class="col-sm-2">
                                ID
                            </div>
                            <div class="col-sm-5">
                                Название продукта
                            </div>
                            <div class="col-sm-5">
                                Компания
                            </div>
                        </div>
                        <div class=""></div>
                        <?php foreach ($products as $product):
                            update_post_views($product->ID, 'views')?>
                            <div class="row">
                                <div class="col-sm-2">
                                    <?= $product->ID ?>
                                </div>
                                <div class="col-sm-5">
                                <?= $product->post_title ?>
                                </div>
                                <div class="col-sm-5">
                                    <?= get_user_by('id', $product->post_author)->display_name; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </section>
            <?php endif; ?>
            <section class=about>
                <div class=container><h2 class=section-title>О компании <span class=accent>TOP</span>METALL</h2></div>
                <div class=about-text>
                    <div class=container>
                        <?php the_content(); ?>
                    </div>
                </div>
            </section>

            <?php
        }
    }
    get_footer();

