<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 13.11.17
     * Time: 23:57
     */

    function ims_admin_scripts()
    {
        wp_enqueue_script('ims_admin', get_template_directory_uri() . '/js/custom-admin.js');
        wp_localize_script('ims_admin', 'ims_admin_ajax', [
            'ajax_url' => admin_url('admin-ajax.php'),
        ]);
    }

    add_action('admin_enqueue_scripts', 'ims_admin_scripts');


    function ims_scripts()
    {
        wp_enqueue_script('ims_main_script', get_template_directory_uri() . '/assets/js/vendors.a81e65f144306c926850.js', false, '', true);
        wp_enqueue_script('ims_app', get_template_directory_uri() . '/app.js', false, '', true);
    }

    add_action('wp_enqueue_scripts', 'ims_scripts');


    function ims_styles()
    {
        wp_enqueue_style('ims_main_style', get_template_directory_uri() . '/assets/css/application.94b06b2d63e82fa2e78d.css', '', '', 'all');
    }

    add_action('wp_enqueue_scripts', 'ims_styles');



