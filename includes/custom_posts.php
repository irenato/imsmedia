<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 13.11.17
     * Time: 23:57
     */

    add_action('init', 'sections_register');

    function sections_register()
    {
        register_post_type('product',
            [
                'label'           => __('Продукты'),
                'singular_label'  => 'product',
                'public'          => true,
                'show_ui'         => true,
                'capability_type' => 'post',
                'hierarchical'    => false,
                'rewrite'         => true,
                'menu_position'   => 5,
                'taxonomies'      => ['post_tag', 'category'],
                'menu_icon'       => 'dashicons-cart',
                'capability_type' => 'product',
                'map_meta_cap' => false,
                'supports'        => [
                    'title',
                    'thumbnail',
                    'editor',
                ],
            ]
        );

//        register_post_type('lead',
//            [
//                'label'           => __('Лиды'),
//                'singular_label'  => 'lead',
//                'public'          => true,
//                'show_ui'         => true,
//                'capability_type' => 'post',
//                'hierarchical'    => false,
//                'rewrite'         => true,
//                'menu_position'   => 5,
//                'menu_icon'       => 'dashicons-clipboard',
//                'capability_type' => 'lead',
//                'map_meta_cap' => false,
//                'supports'        => [
//                    'title',
//                    'thumbnail',
//                    'editor',
//                ],
//            ]
//        );
    }
