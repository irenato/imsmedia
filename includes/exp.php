<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 16.11.17
     * Time: 21:54
     */

    /**
     * @param $column
     *
     * @return mixed
     *
     * добавление колонки на страницу пользователей
     */
    function add_export_column($column)
    {
        $column['leads']  = 'Лиды';
        $column['status'] = 'Статус';
        return $column;
    }

    add_filter('manage_users_columns', 'add_export_column');

    /**
     * @param $val
     * @param $column_name
     * @param $user_id
     *
     * @return string
     * ссылка на скачивание файла экспорта
     */
    function add_export_link($val, $column_name, $user_id)
    {
        switch ($column_name) {
            case 'leads':

                if (get_userdata($user_id)->roles[0] == 'partner') {
                    return show_user_leads_count($user_id) . ' (<a href="?export=leads&user_id=' . $user_id . '">Скачать</a>)   ';
                }
                break;
            case 'status':
                if (get_field('blocked', 'user_' . $user_id)) {
                    return '<span class="submitdelete">Заблокирован</span>';
                }
                break;
        }
    }

    add_filter('manage_users_custom_column', 'add_export_link', 10, 3);

    /**
     * выгрузка лидов
     */
    function export_leads()
    {
        if (isset($_GET['export']) && $_GET['export'] == 'leads') {
            if ($_GET['user_id']) {
                global $wpdb;
                $user_data = get_userdata(esc_sql($_GET['user_id']));
                require_once(get_template_directory() . '/libs/exel/PHPExcel.php');
                $leads = $wpdb->get_results("SELECT * FROM `" . $wpdb->prefix . "ims_leads` 
                                            WHERE `user_id`= " . $user_data->data->ID . "
                                            ORDER BY `id` DESC");

                $xls = new PHPExcel();
                $xls->getProperties()
                    ->setCreator("")
                    ->setLastModifiedBy("")
                    ->setTitle("")
                    ->setSubject("")
                    ->setDescription("")
                    ->setKeywords("office 2007 openxml php")
                    ->setCategory("export file");
                $xls->setActiveSheetIndex(0);
                $rowCount        = 1;
                $cell_definition = [
                    'A' => 'id',
                    'B' => 'имя пользователя',
                    'C' => 'email пользователя',
                    'D' => 'ip клиента',
                    'E' => 'телефон клиента',
                    'F' => 'дата',
                ];
                foreach ($cell_definition as $column => $value)
                    $xls->getActiveSheet()->setCellValue("{$column}1", $value);
                foreach ($leads as $lead) {
                    ++$rowCount;
                    $xls->getActiveSheet()->setCellValue('A' . $rowCount, $user_data->data->ID);
                    $xls->getActiveSheet()->setCellValue('B' . $rowCount, $user_data->data->user_nicename);
                    $xls->getActiveSheet()->setCellValue('C' . $rowCount, $user_data->data->user_email);
                    $xls->getActiveSheet()->setCellValue('D' . $rowCount, $lead->client_ip);
                    $xls->getActiveSheet()->setCellValue('E' . $rowCount, $lead->client_phone);
                    $xls->getActiveSheet()->setCellValue('F' . $rowCount, date('d-m-Y H:i:s', $lead->created_at));
                }
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="leads_report-' . date('d-m-Y') . '.xlsx"');
                header('Cache-Control: max-age=0');
                $objWriter = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
                $objWriter->save('php://output');
                exit;
            }
        }
    }

    add_action('admin_init', 'export_leads');
