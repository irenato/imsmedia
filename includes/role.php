<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 14.11.17
     * Time: 22:37
     */


    /**
     * add custom role
     */
    add_role('partner', 'Партнер', []);

    /**
     * permissions for admin, partner
     */
    function add_theme_caps()
    {
        $administrator = get_role('administrator');
        $partner = get_role('partner');
        $administrator->add_cap('assign_custom_taxes');
        $partner->add_cap('assign_custom_taxes');
        foreach (['publish', 'delete', 'delete_others', 'delete_private', 'delete_published', 'edit', 'edit_others', 'edit_private', 'edit_published', 'read_private'] as $cap) {
            $administrator->add_cap($cap . "_products");
        }
        $partner->add_cap("read");
        foreach (['publish', 'delete', 'delete_published', 'edit', 'edit_published',] as $cap) {
            $partner->add_cap($cap . "_products");
        }
    }
    add_action('admin_init', 'add_theme_caps');