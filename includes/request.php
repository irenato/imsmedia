<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 19.11.17
     * Time: 0:07
     */

    /**
     * удаление преимущества партнеров
     */
    function remove_advantage()
    {
        global $wpdb;
        $data     = [];
        $fields   = [];
        parse_str($_POST['data'], $data);
        foreach ($data as $key => $value)
            $fields[esc_sql($key)] = esc_sql($value);
        if ($fields['old_image'] && file_exists($fields['old_image'])) {
            wp_delete_file($fields['old_image']);
        }
        wp_send_json($wpdb->delete($wpdb->prefix . 'user_category_advantage', [
            'user_id'     => esc_sql($fields['user_id']),
            'category_id' => esc_sql($fields['category_id']),
        ]));
    }

    add_action('wp_ajax_nopriv_remove_advantage', 'remove_advantage');
    add_action('wp_ajax_remove_advantage', 'remove_advantage');