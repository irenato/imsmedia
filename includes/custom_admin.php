<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 13.11.17
     * Time: 23:57
     */

    /**
     * @param $user
     * балланс и лиды на странице пользователя
     */
    function show_user_ballance($user)
    {
        set_query_var('user', $user);
        echo get_template_part('admin-template-parts/section-profile');
    }

    add_action('show_user_profile', 'show_user_ballance');

    /**
     * @param $user_id
     *
     * @return mixed
     *
     * количество лидов пользователя
     */
    function show_user_leads_count($user_id)
    {
        global $wpdb;
        return $wpdb->get_var("SELECT COUNT(*)
                              FROM `" . $wpdb->prefix . "ims_leads` 
                              WHERE `user_id`= " . $user_id . "
                              ORDER BY `id` DESC");
    }

    /**
     * страница преимуществ партнеров (подключение)
     */
    function add_partners_advantage_page()
    {
        add_users_page('Преимущества партнеров', 'Преимущества партнеров', 'read', 'partners-advantage', 'show_partners_advantages_table');
    }

    add_action('admin_menu', 'add_partners_advantage_page');

    /**
     * страница преимуществ партнеров
     */
    function show_partners_advantages_table()
    {
        get_template_part('admin-template-parts/partners-advantage-page');
    }

    /**
     * страница статистики партнеров (подключение)
     */
    function add_partners_statistic_page()
    {
        add_users_page('Статистика партнеров', 'Статистика партнеров', 'read', 'partners-statistic', 'add_partners_statistic_table');
    }

    add_action('admin_menu', 'add_partners_statistic_page');

    /**
     * страница статистики партнеров (подключение)
     */
    function add_profile_statistic_page()
    {
        add_users_page('Статистика партнеров', 'Статистика партнеров', 'read', 'partners-statistic', 'add_partners_statistic_table');
    }

    add_action('admin_menu', 'add_profile_statistic_page');

    /**
     * страница статистики партнеров
     */
    function add_partners_statistic_table()
    {
        get_template_part('admin-template-parts/partners-statistic-page');
    }

    /**
     * страница статистики пользователя
     */
    function add_theme_menu_item()
    {
        add_menu_page("Моя статистика", "Моя статистика", "read", "statistic", "my_statistic", null, 99);
    }

    add_action("admin_menu", "add_theme_menu_item");

    /**
     * страница статистики пользователя
     */
    function my_statistic()
    {
        get_template_part('admin-template-parts/my-statistic-page');
    }


    /**
     * @param $partner_id
     *
     * @return mixed
     *
     * преимущества партнеров в категориях
     */
    function show_partners_advantages($partner_id)
    {
        if (isset($_POST['category_id'])) {
            update_partner_advantage($_POST);
        }
        global $wpdb;
        return $wpdb->get_results("SELECT term_id, name, user_id, description, image
                        FROM `wp_terms` wt 
                        LEFT JOIN `wp_user_category_advantage` wa
                        ON wt.term_id = wa.category_id
                        AND wa.user_id = " . esc_sql($partner_id) . "
                        WHERE wt.term_id IN(
                          SELECT `term_id`
                          FROM `wp_term_taxonomy`
                          WHERE `parent`=0
                          AND `taxonomy`='category'
                        )
                        ORDER BY term_id DESC");
    }

    /**
     * @param $data
     *
     * обновление преимуществ партнера
     */
    function update_partner_advantage($data)
    {
        global $wpdb;
        if (!empty($_FILES["image"]["tmp_name"])) {
            $file = wp_handle_upload($_FILES["image"], ['test_form' => FALSE]);
            if ($data['old_image'] && file_exists($data['old_image'])) {
                wp_delete_file($data['old_image']);
            }
        }

        $wpdb->delete($wpdb->prefix . 'user_category_advantage', [
            'user_id'     => esc_sql($data['user_id']),
            'category_id' => esc_sql($data['category_id']),
        ]);

        $wpdb->insert($wpdb->prefix . 'user_category_advantage', [
            'user_id'     => esc_sql($data['user_id']),
            'category_id' => esc_sql($data['category_id']),
            'description' => esc_sql($data['description']),
            'image'       => $file ? $file["url"] : null,
        ]);
    }

    /**
     * theme options
     */
    if (function_exists('acf_add_options_page')) {

        acf_add_options_page([
            'page_title' => 'Theme General Settings',
            'menu_title' => 'Theme Settings',
            'menu_slug'  => 'theme-general-settings',
            'capability' => 'edit_posts',
            'redirect'   => false,
        ]);

        acf_add_options_sub_page([
            'page_title'  => 'Theme Header Settings',
            'menu_title'  => 'Header',
            'parent_slug' => 'theme-general-settings',
        ]);

        acf_add_options_sub_page([
            'page_title'  => 'Theme Footer Settings',
            'menu_title'  => 'Footer',
            'parent_slug' => 'theme-general-settings',
        ]);

    }

    function show_categories_title($post_id){
        $result = [];
        $categories = get_the_category($post_id);
        foreach ($categories as $category){
            if(property_exists($category, 'name'))
            array_push($result, $category->name);
        }
        return count($result) > 0 ? implode(', ', $result) : ' - ';
    }



