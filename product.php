<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 21.11.17
     * Time: 21:08
     */
    /**
     * Template name: Product
     */
    get_header();
?>


    <section class=product>
        <div class=container>
            <div class=product-title> Результаты: <span class=product-title__name><?= show_search_param() ?></span> <span class=product-title__img>
                    <img src="<?= get_template_directory_uri() ?>/assets/static/png/product-1_c4aa80843aebbb2a32ffe0febf9070fd.png" alt=""></span>
            </div>
            <form action="" class=product-form>
                <div class=row>
                    <div class="col-md-3 col-sm-4"><select name="" id="" class=sort-list>
                            <option value="">Выберите Вид</option>
                        </select></div>
                    <div class="col-md-3 col-sm-4"><select name="" id="" class=sort-list>
                            <option value="">Выберите Диаметр</option>
                        </select></div>
                    <div class="col-md-3 col-sm-4">
                        <button type=submit class="btn btn_white">Показать</button>
                    </div>
                </div>
            </form>
            <div class=product-table-wrap>
                <table class=product-table>
                    <thead>
                    <tr>
                        <td>Металлобаза:</td>
                        <td>Рейтинг:</td>
                        <td colspan=2>Условия:</td>
                        <td colspan=2>Актуальные цены:</td>
                        <td>Контакты:</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $products = get_data();
                        if ($products):
                            foreach ($products as $product):
                                //  увеличиваю счетчик показов
                                update_post_views($product->ID);
                                $author = get_user_by('id', $product->post_author); ?>
                                <tr>
                                    <td><?= $author->display_name ?></td>
                                    <td> 4 из 5
                                        <ul class=rating>
                                            <li><i>&#xE808</i></li>
                                            <li><i>&#xE808</i></li>
                                            <li><i>&#xE808</i></li>
                                            <li><i>&#xE808</i></li>
                                            <li class=dark><i>&#xE808</i></li>
                                        </ul>
                                    </td>


                                    <?php $partner_advantages = isset($_GET['category']) ? show_partner_advantages($author->ID, $_GET['category']) : ''; ?>
                                    <?php if ($partner_advantages): ?>
                                        <?php foreach ($partner_advantages as $partner_advantage): ?>
                                            <td>
                                                <i class=condition-icon>&#xE806</i>
                                                <img src="<?= $partner_advantage->image ?>" style="width: 75px; height: 75px" alt="">
                                            </td>
                                            <td>
                                                <i class=condition-icon>&#xE806</i>
                                                <?= $partner_advantage->description ?>
                                            </td>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <td> -</td>
                                        <td> -</td>
                                    <?php endif; ?>

                                    <td>Цены обновлены: <br> <?= date('d.m.Y', strtotime($product->post_modified)) ?> в <?= date('H:i', strtotime($product->post_modified)) ?></td>
                                    <td>Цена: <span class=product-price><?= get_field('price', $product->ID) ?> грн.</span></td>
                                    <td> Звоните сейчас: <span class=product-phone><?= get_phones($author->ID); ?></span>
                                        <?php if (get_field('price_list', 'user_' . $author->ID)): ?>
                                            <a href="<?= get_field('price_list', 'user_' . $author->ID) ?>" download class="btn product-btn">
                                                Скачать полный прайс лист
                                                <span><i>&#xE807</i></span>
                                            </a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                        endif;
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
<?php

    get_footer();