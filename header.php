<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 13.11.17
     * Time: 23:53
     */
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset=UTF-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta name=viewport content="width=device-width,initial-scale=1">
    <title><?= wp_get_document_title('|', true, 'right'); ?></title>
    <link rel="shortcut icon" href=# type=image/x-icon>
    <?php wp_head(); ?>
</head>
<body id=body class="<?= !empty($_GET["utm_campaign"]) ? str_replace(["_poisk", "_kms", "-poisk_g", "-kms"], "", $_GET["utm_campaign"]) : "" ?>">
<header class=header>
    <div class=top-line>
        <div class=container>
            <div class=row>
                <div class="col-md-6 col-md-push-3 col-sm-5 col-sm-push-3 col-center"><a href=/ class=logo>
                        <div class=logo__top><span class=accent>TOP</span>Metall</div>
                        <div class=logo__bottom>Актуальные цены на металлопрокат</div>
                    </a></div>
                <div class="col-md-3 col-md-pull-6 col-sm-3 col-sm-pull-5">
                    <div class=header-select><select name="" id="" class=sort-list>
                            <option value="">Харьков</option>
                            <option value="">не Харьков</option>
                        </select></div>
                </div>
                <div class="col-md-3 col-sm-4 col-right"><a href=javascript:void(0) class="btn header__btn">Разместить заявку</a></div>
            </div>
        </div>
    </div>
    <div class=container>
        <form action="<?= get_page_link(2) ?>" method="get" class=search-form>
            <input type=text class=input name="search" placeholder="Что Вы хотите купить?" value="<?= isset($_GET['search']) ? $_GET['search'] : '' ?>">
            <button type=submit class=btn>
                <i>&#xE800</i>
            </button>
        </form>
        <?php if (is_page_template('home.php')): ?>
            <div class=advantages>
                <img src="<?= get_template_directory_uri() ?>/assets/static/png/header-product_92eee347e50c112a9532b3616f8b8f96.png" alt="" class=header-product>
                <ul class="advantages-list clearfix">
                    <?php $advantages = get_field('advantage', 111); ?>
                    <?php if ($advantages): ?>
                        <?php foreach ($advantages as $advantage): ?>
                            <li class=advantages-item><i>&#xE80a</i> <span><?= $advantage['title'] ?></span></li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div>
        <?php endif; ?>
    </div>
</header>