/**
 * Created by macbook on 18.11.17.
 */
jQuery(document).ready(function ($) {
    /**
     * сортировка пользователей
     */
    $('select[name=partner]').on('change', function () {
        $(this).closest('form').submit();
    })

    /**
     * обнуление преимущества
     */
    $('.js_clear_advantage').on('click', function (e) {
        e.preventDefault();
        var _this = $(this);
        console.log($(_this).closest('tr').find('form').serialize());
        $.ajax({
            url    : '/wp-admin/admin-ajax.php',
            type   : 'POST',
            data   : {
                'action': 'remove_advantage',
                'data'  : $(_this).closest('tr').find('form').serialize(),
            },
            success: function (responce) {
                if(responce == 1)
                    location.reload();

            },
        })
    })
})
