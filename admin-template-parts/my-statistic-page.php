<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 23.11.17
     * Time: 0:08
     */

    ?>

<table class="form-table">
    <tbody>
    <tr>
        <th>id</th>
        <th>Дата</th>
        <th>Товар</th>
        <th>Категории</th>
        <th>К-во отображений</th>
    </tr>
    <?php $products = get_posts([
        'author'         => $user->data->ID,
        'order'          => 'ASC',
        "post_type"      => "product",
        'order'          => 'DESC',
        'orderby'        => 'meta_value',
        'meta_key'       => 'views',
        'posts_per_page' => -1,
    ]);

        if ($products) {
            foreach ($products as $product) {
                ?>
                <tr>
                    <td> <?= $product->ID ?> </td>
                    <td> <?= $product->post_modified ?> </td>
                    <td> <?= $product->post_title ?> </td>
                    <td> <?= show_categories_title($product->ID); ?> </td>
                    <td> <?= get_post_meta($product->ID, 'views', true) ?></td>
                    <td></td>
                </tr>
            <?php }
        } ?>
    </tbody>
</table>
