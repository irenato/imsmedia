<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 22.11.17
     * Time: 23:04
     */

?>

<table class="form-table">
    <tbody>
    <tr>
        <th>Партнер</th>
        <td>
            <form action="<?= admin_url('users.php') ?>" method="GET">
                <input type="hidden" name="page" value="partners-statistic">
                <?php $partners = get_users(['role' => 'partner']);
                    if ($partners) {
                        ?>
                        <select name="partner">
                            <?php $i == 0;
                                foreach ($partners as $partner) {
                                    if ($i == 0) {
                                        $parner_id = $_GET['partner'] ? $_GET['partner'] : $partner->data->ID;
                                        ++$i;
                                    }
                                    ?>
                                    <option value="<?= $partner->data->ID ?>"
                                        <?= $partner->data->ID == $_GET['partner'] ? 'selected' : '' ?>>
                                        <?= $partner->data->user_nicename ?>
                                    </option>
                                <?php } ?>
                        </select>
                    <?php } ?>
            </form>
        </td>
    </tr>
    </tbody>
</table>

<table class="form-table">
    <tbody>
    <tr>
        <th>id</th>
        <th>Дата</th>
        <th>Товар</th>
        <th>Категории</th>
        <th>К-во отображений</th>
    </tr>
    <?php $products = get_posts([
        'author'         => $parner_id,
        'order'          => 'ASC',
        "post_type"      => "product",
        'order'          => 'DESC',
        'orderby'        => 'meta_value',
        'meta_key'       => 'views',
        'posts_per_page' => -1,
    ]);

        if ($products) {
            foreach ($products as $product) {
                ?>
                <tr>
                    <td> <?= $product->ID ?> </td>
                    <td> <?= $product->post_modified ?> </td>
                    <td> <?= $product->post_title ?> </td>
                    <td> <?= show_categories_title($product->ID); ?> </td>
                    <td> <?= get_post_meta($product->ID, 'views', true) ?></td>
                    <td></td>
                </tr>
            <?php }
        } ?>
    </tbody>
</table>
