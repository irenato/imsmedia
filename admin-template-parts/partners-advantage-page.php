<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 18.11.17
     * Time: 18:45
     */

?>

<table class="form-table">
    <tbody>
    <tr>
        <th>Партнер</th>
        <td>
            <form action="<?= admin_url('users.php') ?>" method="GET">
                <input type="hidden" name="page" value="partners-advantage">
                <?php $partners = get_users(['role' => 'partner']);
                    if ($partners) {
                        ?>
                        <select name="partner">
                            <?php $i == 0;
                                foreach ($partners as $partner) {
                                    if ($i == 0) {
                                        $parner_id = $_GET['partner'] ? $_GET['partner'] : $partner->data->ID;
                                        ++$i;
                                    }
                                    ?>
                                    <option value="<?= $partner->data->ID ?>"
                                        <?= $partner->data->ID == $_GET['partner'] ? 'selected' : '' ?>>
                                        <?= $partner->data->user_nicename ?>
                                    </option>
                                <?php } ?>
                        </select>
                    <?php } ?>
            </form>
        </td>
    </tr>
    </tbody>
</table>

<table class="form-table">
    <tbody>
    <tr>
        <th>Категория</th>
        <th>Преимущество</th>
        <th>Изображение</th>
        <th>Действие</th>
    </tr>
    <?php $partner_advantages = show_partners_advantages($parner_id);
        if ($partner_advantages) {
            foreach ($partner_advantages as $partner_advantage) {
                ?>
                <tr>
                    <form action="<?= admin_url('users.php?page=partners-advantage&partner=' . $parner_id) ?>" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="category_id" value="<?= $partner_advantage->term_id ?>">
                        <input type="hidden" name="user_id" value="<?= $parner_id ?>">
                        <input type="hidden" name="old_image" value="<?= $partner_advantage->image ?>">
                        <td> <?= $partner_advantage->name ?> </td>
                        <td><textarea name="description" required cols="70" rows="10"><?= $partner_advantage->description ?></textarea></td>
                        <td><img src="<?= $partner_advantage->image ?>" style="width: 120px; height: 120px" alt=""><input type="file" name="image" accept="image/png"></td>
                        <td><input type="submit" class="button button-primary" value="Сохранить">
                            <p><a href="#" class="js_clear_advantage">Очистить</a></p>
                        </td>
                    </form>
                </tr>
            <?php }
        } ?>
    </tbody>
</table>

