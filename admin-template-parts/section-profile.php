<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 19.11.17
     * Time: 0:21
     */
    ?>

<table class="form-table"><tbody>
    <tr>
        <th>Балланс</th>
        <td><?= get_field('ballance', 'user_' . $user->data->ID) ?></td>
    </tr>
    <tr>
        <th>Стоимость лида</th>
        <td><?= get_field('cost_lead', 'user_' . $user->data->ID) ?></td>
    </tr>
    <tr>
        <th>Лиды</th>
        <td><?= show_user_leads_count($user->data->ID) ?></td>
        <td><a href="?export=leads&user_id=<?= $user->data->ID ?>">Скачать отчет</a></td>
    </tr>
    </tbody>
</table>
